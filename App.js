import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  ScrollView,
  Text,
  View,
  Button,
  TouchableOpacity,
} from "react-native";
import { useState, useEffect } from "react";

export default function App() {
  const [count, setCount] = useState(0);
  const [dado, setDado] = useState(0);
  function rodarDado(){
    setDado(Math.floor(Math.random() * 6) + 1);
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>
        <Text>Cade o mundial?!?!?!?!</Text>

        <Text>Clique para contar: {dado} </Text>
        {/* <TouchableOpacity
          style={styles.teste}
          onPress={() => setCount(count + 1)}
        >
          <Text>Clique!</Text>
        </TouchableOpacity> */}

        <TouchableOpacity
          style={styles.teste}
          onPress={() => rodarDado()}
        >
          <Text>Jogue o dado!</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "pink",
    alignItems: "center",
    justifyContent: "center",
  },

  teste: {
    color: "",
    backgroundColor: "blue",
    width: 90,
    borderRadius: 25,
    alignItems: "center",
  },
});
